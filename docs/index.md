# Welcome

This is a test build of the website using [mkdocs.org](http://mkdocs.org).
Markdown style notation drives this website.

## Project deploys

Feel free to check the [source code](https://gitlab.com/cordain/cordain-gitlab-io)

You can navigate to following pages for my project examples:

- [Group expense calculator](expense-calc)
- [Hexwarheads game](hexwarheads)